import gitlab
import pandas as pd
import datetime
from zoneinfo import ZoneInfo
import os

GITLAB_URL = os.getenv('CI_SERVER_URL')
TOKEN = os.getenv('TOKEN')
GITLAB_GROUP_ID = os.getenv('GITLAB_GROUP_ID')

html_template = '''
<html>
<head>
<meta charset="utf-8">
<title>GitLab Pipeline Monitor</title>
</head>
<body>
<h1>GitLab Pipeline Monitor</h1>
GitLab URL: {GITLAB_URL} <br>
GitLab Group: {GITLAB_GROUP} <br>
Updated: {UPDATED_AT} <br>
Summary:
{SUMMARY}
<hr>
{TABLE}
</body>
</html>
'''


def main():
    attr = ['project_id', 'project_name', 'pipeline_updated_at', 'pipeline_status']
    gl = gitlab.Gitlab(url=GITLAB_URL, private_token=TOKEN)
    df_all = pd.DataFrame(columns=attr)
    gl_group = gl.groups.get(id=GITLAB_GROUP_ID)
    dt_now = datetime.datetime.now(ZoneInfo("Asia/Tokyo")).strftime('%Y年%m月%d日 %H:%M:%S')
    for i in gl_group.projects.list(iterator=True):
        data = {x: '' for x in attr}
        data['project_id'] = [i.id]
        data['project_name'] = [i.name]
        pipelines = gl.projects.get(i.id).pipelines.list(order_by='updated_at', sort='desc')
        if len(pipelines) == 0:
            data['pipeline_updated_at'] = ['not run']
            data['pipeline_status'] = ['not run']
        else:
            latest_pipeline = pipelines[0]
            data['pipeline_updated_at'] = [latest_pipeline.updated_at]
            data['pipeline_status'] = [latest_pipeline.status]
        df_tmp = pd.DataFrame(data, columns=attr)
        df_all = pd.concat([df_all, df_tmp], axis=0)
    df_counts = pd.DataFrame(df_all['pipeline_status'].value_counts())
    html = html_template.format(
        GITLAB_URL=GITLAB_URL,
        GITLAB_GROUP=gl_group.name,
        UPDATED_AT=dt_now,
        SUMMARY=df_counts.to_html(),
        TABLE=df_all.to_html(index=False))

    with open('index.html', mode='w') as f:
        f.write(html)


if __name__ == '__main__':
    main()
