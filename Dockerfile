FROM python:3.11
RUN pip install --no-cache-dir python-gitlab==3.13.0
RUN pip install --no-cache-dir pandas==1.5.3